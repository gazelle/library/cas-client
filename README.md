#Gazelle Cas Client

This module is used by Gazelle web application to communicate with the Apereo CAS

## How to

###Compilation

```bash
mvn clean install
```


###Usage

1. Add as dependency in your ejb
    ```xml
     <dependency>
         <groupId>net.ihe.gazelle</groupId>
         <artifactId>gazelle-cas-client</artifactId>
         <version>${gazelle.cas.client.version}</version>
     </dependency>
     ```
     
1. Update your web.xml like :
    ```xml
    <context-param>
        <param-name>configurationStrategy</param-name>
        <param-value>PROPERTY_FILE</param-value>
    </context-param>

    <context-param>
        <param-name>configFileLocation</param-name>
        <param-value>/opt/gazelle/cas/file.properties</param-value>
    </context-param>

    <filter>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <listener>
        <listener-class>org.jasig.cas.client.session.SingleSignOutHttpSessionListener</listener-class>
    </listener>

    <filter>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <url-pattern>/cas/login</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <filter-class>net.ihe.gazelle.cas.client.authentication.LogoutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <url-pattern>/cas/logout.seam</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Validation Filter</filter-name>
        <filter-class>org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter
        </filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Validation Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    ```

1. Update your pages.xml like :
    ```xml
    <page view-id="*">
        <navigation from-action="#{identity.logout}">
            <rule if="#{applicationPreferenceManager.getBooleanValue('cas_enabled')}">
                <redirect view-id="/cas/logout.xhtml"/>
            </rule>
            <rule if="#{!applicationPreferenceManager.getBooleanValue('cas_enabled')}">
                <redirect view-id="/home.xhtml"/>
            </rule>
        </navigation>
    </page>
    <page view-id="/cas/login">
        <navigation>
            <redirect view-id="/home.xhtml"/>
        </navigation>
    </page>
    ```
    
 1. Create login/logout buttons like :
    ```xml
    <h:outputLink id="loginLink" value="#{applicationPreferenceManager.getStringValue('application_url')}/cas/login">
        <h:outputText value="#{messages['net.ihe.gazelle.evs.CASLogin']}"/>
        <f:param name="request" value="#{request.requestURL}" disable="#{request.queryString != null}"/>
        <f:param name="request" value="#{request.requestURL}?#{request.queryString}" disable="#{request.queryString == null}"/>
    </h:outputLink>
    ```
    
    ```xml
    <h:commandLink immediate="true"
       action="#{identity.logout}"
       value="#{messages['gazelle.users.registration.Logout']}"/>
    ```
