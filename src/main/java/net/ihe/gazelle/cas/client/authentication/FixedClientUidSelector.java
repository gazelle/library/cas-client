package net.ihe.gazelle.cas.client.authentication;

import org.apache.commons.lang3.RandomStringUtils;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Named("clientUidSelector")
//TODO SEE IF IT IS USED AND HOW
public class FixedClientUidSelector /*extends ClientUidSelector */{

    private static final long serialVersionUID = 5237089512441072594L;
    private static final int LENGHT = 50;
    private String clientUid;

    private static final int DEFAULT_MAX_AGE = 31536000; // 1 year in seconds
    private boolean cookieEnabled;
    private int cookieMaxAge = DEFAULT_MAX_AGE;
    private String cookiePath= "/";

    @PostConstruct
    public void onCreate() {
        String requestContextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        setCookiePath(requestContextPath);
        setCookieMaxAge(-1);
        setCookieEnabled(true);
        clientUid = getCookieValue();
    }

    public void setCookiePath(String cookiePath){
        this.cookiePath = cookiePath;
    }

    public void setCookieMaxAge(int cookieMaxAge){
        this.cookieMaxAge = cookieMaxAge;
    }

    public boolean isCookieEnabled() {
        return cookieEnabled;
    }

    public void setCookieEnabled(boolean isCookieEnabled){
        this.cookieEnabled = isCookieEnabled;
    }

    public void seed() {
        if (!isSet()) {
            clientUid = RandomStringUtils.random(LENGHT, true, true);
            setCookieValueIfEnabled(clientUid);
        }
    }

    public boolean isSet() {
        return clientUid != null;
    }

    public String getClientUid() {
        return clientUid;
    }

    protected String getCookieName() {
        return "javax.faces.ClientToken";
    }

    /**
     * Set the cookie
     */
    private void setCookieValueIfEnabled(String value) {
        FacesContext ctx = FacesContext.getCurrentInstance();

        if (isCookieEnabled() && (ctx != null)) {
            HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();
            Cookie cookie = new Cookie(getCookieName(), value);
            cookie.setMaxAge(getCookieMaxAge());
            cookie.setPath(getCookiePath());
            // Fix for IE7
            cookie.setVersion(1);
            response.addCookie(cookie);
        }
    }

    private String getCookieValue() {
        Cookie cookie = getCookie();
        return cookie==null ? null : cookie.getValue();
    }

    private Cookie getCookie() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (ctx != null) {
            return (Cookie) ctx.getExternalContext().getRequestCookieMap()
                    .get( getCookieName() );
        } else {
            return null;
        }
    }

    public int getCookieMaxAge() {
        return cookieMaxAge;
    }

    public String getCookiePath() {
        return cookiePath;
    }
}