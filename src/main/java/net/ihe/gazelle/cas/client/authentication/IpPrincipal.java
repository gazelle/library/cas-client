package net.ihe.gazelle.cas.client.authentication;

import java.security.Principal;

/**
 * <p>IpPrincipal class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class IpPrincipal implements Principal {

	private String name;

	/**
	 * <p>Constructor for IpPrincipal.</p>
	 *
	 * @param name a {@link java.lang.String} object.
	 */
	public IpPrincipal(String name) {
		super();
		this.name = name;
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return name;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IpPrincipal other = (IpPrincipal) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "IpPrincipal [name=" + name + "]";
	}

}
