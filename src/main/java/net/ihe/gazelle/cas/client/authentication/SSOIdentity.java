package net.ihe.gazelle.cas.client.authentication;

import net.ihe.gazelle.preferences.GenericConfigurationManager;
import org.apache.commons.lang3.StringUtils;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.authentication.SimpleGroup;
import org.jasig.cas.client.authentication.SimplePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.auth.Subject;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.Principal;
import java.security.acl.Group;
import java.util.*;

/**
 * <p>SSOIdentity class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Named("ssoIdentity")
@SessionScoped
public class SSOIdentity implements Serializable {

	private static final long serialVersionUID = -631532323964539777L;

	/** Constant <code>AUTHENTICATED_USER="org.jboss.seam.security.management.auth"{trunked}</code> */
	public static final String AUTHENTICATED_USER = "org.jboss.seam.security.management.authenticatedUser";
	/** Constant <code>EVENT_USER_AUTHENTICATED="org.jboss.seam.security.management.user"{trunked}</code> */
	public static final String EVENT_USER_AUTHENTICATED = "org.jboss.seam.security.management.userAuthenticated";
	private static final String SILENT_LOGIN = "org.jboss.seam.security.silentLogin";

	private static Logger log = LoggerFactory.getLogger(SSOIdentity.class);

	/** Constant <code>INSTITUTION_KEYWORD_KEY="institution_keyword"</code> */
	public static final String INSTITUTION_KEYWORD_KEY = "institution_keyword";
	/** Constant <code>INSTITUTION_NAME_KEY="institution_name"</code> */
	public static final String INSTITUTION_NAME_KEY = "institution_name";

	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
	}

	public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init) {
	}

	@Inject
	private GenericConfigurationManager applicationConfigurationManager;
	private Principal principal;
	private Subject subject = new Subject();
	private List<String> preAuthenticationRoles = new ArrayList<String>();

    private String username;
    private String password;

	public String login() {

		try {
			preAuthenticate();
			if (applicationConfigurationManager.isWorksWithoutCas()
					&& !applicationConfigurationManager.getIpLogin()) {
				// Any user is granted as admin
                return grantUser();
			} else if (applicationConfigurationManager.isWorksWithoutCas()
					&& applicationConfigurationManager.getIpLogin()) {
				Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
				if (request instanceof ServletRequest) {
					ServletRequest servletRequest = (ServletRequest) request;
					String ipRegexp = applicationConfigurationManager.getIpLoginAdmin();
					if (ipRegexp == null) {
						ipRegexp = "";
					}
					String remoteAddr = servletRequest.getRemoteAddr();
					if (remoteAddr.matches(ipRegexp)) {
                        return grantUser();
					}
				}
			}

			AttributePrincipal attributePrincipal = null;
			Principal casPrincipal = getCASPrincipal();
			if (casPrincipal instanceof AttributePrincipal) {
				attributePrincipal = (AttributePrincipal) casPrincipal;
			}

			if (attributePrincipal == null) {
				return "notLoggedIn";
			} else if (casPrincipal.getName() != null) {
				preAuthenticate();

				String username = casPrincipal.getName();

				log.info("Found CAS principal for " + username + ": authenticated");
				Map attributes = attributePrincipal.getAttributes();
				Set<Map.Entry> entrySet = attributes.entrySet();
				for (Map.Entry object : entrySet) {
					log.info("       " + object.getKey() + " = " + object.getValue());
				}

				acceptExternallyAuthenticatedPrincipal(casPrincipal);
				if ((attributes != null) && (attributes.get("role_name") != null)) {
					String roles = (String) attributes.get("role_name");
					StringTokenizer st = new StringTokenizer(roles, "[,]");
					String role;
					while (st.hasMoreElements()) {
						role = (String) st.nextElement();
						role = StringUtils.trimToNull(role);
						if (role != null) {
							addRole(role);
						}
					}
				}
                this.username = username;
				postAuthenticate();
				return "loggedIn";
			}

		} catch (Throwable e) {
			principal = null;
			subject = new Subject();
			username = null;
			password = null;
			throw new RuntimeException(e);
		}

		return null;
	}

	protected void postAuthenticate() {
		// Populate the working memory with the user's principals
		for ( Principal p : subject.getPrincipals() ) {
			if ( !(p instanceof Group)) {
				if (principal == null) {
					principal = p;
					break;
				}
			}
		}
		if (!preAuthenticationRoles.isEmpty() && this.principal != null) {
			for (String role : preAuthenticationRoles) {
				addRole(role);
			}
			preAuthenticationRoles.clear();
		}

		this.password = null;
	}

	protected void preAuthenticate() {
		preAuthenticationRoles.clear();
	}

	public boolean addRole(final String role) {
		if (role == null || "".equals(role)) return false;

		if (this.principal == null) {
			preAuthenticationRoles.add(role);
			return false;
		} else {
			for ( Group sg : subject.getPrincipals(Group.class) ) {
				if ( "Roles".equals( sg.getName() ) ) {
					return sg.addMember(new SimplePrincipal(role));
				}
			}

			SimpleGroup roleGroup = new SimpleGroup("Roles");
			roleGroup.addMember(new SimplePrincipal(role));
			subject.getPrincipals().add(roleGroup);
			return true;
		}
	}

	public void acceptExternallyAuthenticatedPrincipal(Principal principal) {
		subject.getPrincipals().add(principal);
		this.principal = principal;
	}

    private String grantUser() {
        String username = "admin";
        acceptExternallyAuthenticatedPrincipal(new IpPrincipal(username));
        addRole("admin_role");
        this.username = username;
        postAuthenticate();
        return "loggedIn";
    }

    private Principal getCASPrincipal() {
		FacesContext fc = FacesContext.getCurrentInstance();
		if (fc == null) {
			return null;
		}
		final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		final HttpSession session = request.getSession(false);
		final Assertion assertion = (Assertion) (session == null ? request
				.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) : session
				.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
		return assertion == null ? null : assertion.getPrincipal();
	}

	public boolean isLoggedIn() {
		if (this.principal == null) {
			if (getCASPrincipal() != null) {
				login();
			}
		}
		return this.principal != null;
	}

	public void logout() {
		Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (request instanceof ServletRequest) {
			ServletRequest servletRequest = (ServletRequest) request;
			servletRequest.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
		}
		Object session = FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if (session instanceof HttpSession) {
			HttpSession httpSession = (HttpSession) session;
			httpSession.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
		}
		principal = null;
		subject = new Subject();
		username = null;
		password = null;
	}

	/**
	 * <p>getCompanyKeyword.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCompanyKeyword(){
		return getCustomAttribute(INSTITUTION_KEYWORD_KEY);
	}

	public String getUsername(){
		return username;
	}

	/**
	 * <p>getCompanyName.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCompanyName(){
		return getCustomAttribute(INSTITUTION_NAME_KEY);
	}

	private String getCustomAttribute(String key){
		AttributePrincipal attributePrincipal = null;
		Principal casPrincipal = getCASPrincipal();
		if (casPrincipal instanceof AttributePrincipal) {
			attributePrincipal = (AttributePrincipal) casPrincipal;
			Map attributes = attributePrincipal.getAttributes();
			return (String) attributes.get(key);
		} else {
			return null;
		}
	}

	public boolean hasRole(String role) {
		for ( Group sg : subject.getPrincipals(Group.class)) {
			if ( "Roles".equals( sg.getName() ) ) {
				return sg.isMember(new SimplePrincipal(role));
			}
		}
		return false;
	}

}
