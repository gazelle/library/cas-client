package net.ihe.gazelle.cas.client.authentication;

import javax.inject.Named;
import java.io.Serializable;

/**
 * <p>UserPermission class.</p>
 *
 * @author abderrazek boufahja
 * @version $Id: $Id
 */
//TODO Migrate seam permission annotations
@Named("userPermission")
public class UserPermission implements Serializable
{
   private static final long serialVersionUID = -5628863031792429938L;
   
   private Long id;
   private String recipient;
   private String target;
   private String action;
   private String discriminator;
   
   /**
    * <p>Getter for the field <code>id</code>.</p>
    *
    * @return a {@link java.lang.Long} object.
    */
   public Long getId()
   {
      return id;
   }
   
   /**
    * <p>Setter for the field <code>id</code>.</p>
    *
    * @param id a {@link java.lang.Long} object.
    */
   public void setId(Long id)
   {
      this.id = id;
   }
   
   /**
    * <p>Getter for the field <code>recipient</code>.</p>
    *
    * @return a {@link java.lang.String} object.
    */
//   @PermissionUser
//   @PermissionRole
   public String getRecipient()
   {
      return recipient;
   }
   
   /**
    * <p>Setter for the field <code>recipient</code>.</p>
    *
    * @param recipient a {@link java.lang.String} object.
    */
   public void setRecipient(String recipient)
   {
      this.recipient = recipient;
   }
   
   /**
    * <p>Getter for the field <code>target</code>.</p>
    *
    * @return a {@link java.lang.String} object.
    */
//   @PermissionTarget
   public String getTarget()
   {
      return target;
   }
   
   /**
    * <p>Setter for the field <code>target</code>.</p>
    *
    * @param target a {@link java.lang.String} object.
    */
   public void setTarget(String target)
   {
      this.target = target;
   }
   
   /**
    * <p>Getter for the field <code>action</code>.</p>
    *
    * @return a {@link java.lang.String} object.
    */
//   @PermissionAction
   public String getAction()
   {
      return action;
   }
   
   /**
    * <p>Setter for the field <code>action</code>.</p>
    *
    * @param action a {@link java.lang.String} object.
    */
   public void setAction(String action)
   {
      this.action = action;
   }
   
   /**
    * <p>Getter for the field <code>discriminator</code>.</p>
    *
    * @return a {@link java.lang.String} object.
    */
//   @PermissionDiscriminator
   public String getDiscriminator()
   {
      return discriminator;
   }
   
   /**
    * <p>Setter for the field <code>discriminator</code>.</p>
    *
    * @param discriminator a {@link java.lang.String} object.
    */
   public void setDiscriminator(String discriminator)
   {
      this.discriminator = discriminator;
   }

}
