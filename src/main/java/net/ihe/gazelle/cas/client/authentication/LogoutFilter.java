package net.ihe.gazelle.cas.client.authentication;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jasig.cas.client.Protocol;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class LogoutFilter extends AbstractCasFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LogoutFilter.class);

    private String casServerUrl;
    private String applicationUrl;

    public LogoutFilter() {
        super(Protocol.CAS2);
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getCasServerUrl() {
        return casServerUrl;
    }

    public void setCasServerUrl(String casServerUrl) {
        this.casServerUrl = casServerUrl;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException,
            ServletException {
        StringBuilder logoutUrlBuilder = new StringBuilder();
        try {
            logoutUrlBuilder.append(getCasServerUrl());
            logoutUrlBuilder.append("/logout");
            if (getApplicationUrl() != null) {
                logoutUrlBuilder.append("?service=");
                final String encodedKey = URLEncoder.encode(getApplicationUrl(), StandardCharsets.UTF_8.toString());
                logoutUrlBuilder.append(encodedKey);
                LOG.debug("url to reach : " + logoutUrlBuilder.toString());
            }
            ((HttpServletResponse) servletResponse).sendRedirect(logoutUrlBuilder.toString());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        }
    }

    @Override
    public void init() {
        super.init();
        setCasServerUrl(getString(ConfigurationKeys.CAS_SERVER_URL_PREFIX));
        reloadParameters();
    }

    private void reloadParameters() {
        // Create a seam context if needed
        setApplicationUrl(PreferenceService.getString("application_url"));
    }
}
