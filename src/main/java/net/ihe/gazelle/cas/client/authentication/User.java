package net.ihe.gazelle.cas.client.authentication;

import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * <p>User class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
//TODO Migrate seam IDM annotations
@Named("user")
public class User implements Serializable{

	private static final long serialVersionUID = 6368734442192368866L;
	
	private String username;
	private String password;
	private String lastname;
	private String firstname;
	private List<UserRole> roles;

	/**
	 * <p>Getter for the field <code>username</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
//	@UserPrincipal
	public String getUsername(){
		return username;
	}
	
	/**
	 * <p>Getter for the field <code>lastname</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
//	@UserFirstName
	public String getLastname(){
		return lastname;
	}
		
	/**
	 * <p>Getter for the field <code>firstname</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
//	@UserLastName
	public String getFirstname(){
		return firstname;
	}
	
	/**
	 * <p>Getter for the field <code>password</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
//	@UserPassword
	public String getPassword(){
		return password;
	}
	
	/**
	 * <p>Setter for the field <code>username</code>.</p>
	 *
	 * @param inUsername a {@link java.lang.String} object.
	 */
	public void setUsername(String inUsername){
		this.username = inUsername;
	}
	
	/**
	 * <p>Setter for the field <code>password</code>.</p>
	 *
	 * @param inPassword a {@link java.lang.String} object.
	 */
	public void setPassword(String inPassword){
		this.password = inPassword;
	}


	/**
	 * <p>Setter for the field <code>lastname</code>.</p>
	 *
	 * @param lastname a {@link java.lang.String} object.
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	/**
	 * <p>Setter for the field <code>firstname</code>.</p>
	 *
	 * @param firstname a {@link java.lang.String} object.
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * <p>Getter for the field <code>roles</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
//	@UserRoles
	public List<UserRole> getRoles() {
		return roles;
	}

	/**
	 * <p>Setter for the field <code>roles</code>.</p>
	 *
	 * @param roles a {@link java.util.List} object.
	 */
	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}
}
